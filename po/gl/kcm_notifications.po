# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2019, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-16 01:38+0000\n"
"PO-Revision-Date: 2023-08-26 03:46+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: kcm.cpp:73
#, kde-format
msgid "Toggle do not disturb"
msgstr "Conmutar non molestar"

#: sourcesmodel.cpp:393
#, kde-format
msgid "Other Applications"
msgstr "Outras aplicacións"

#: ui/ApplicationConfiguration.qml:91
#, kde-format
msgid "Show popups"
msgstr "Amosar as xanelas emerxentes"

#: ui/ApplicationConfiguration.qml:105
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Amosar no modo de non molestar"

#: ui/ApplicationConfiguration.qml:118 ui/main.qml:147
#, kde-format
msgid "Show in history"
msgstr "Amosar no historial"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid "Show notification badges"
msgstr "Amosar os emblemas de notificacións"

#: ui/ApplicationConfiguration.qml:164
#, kde-format
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "Configurar os eventos"

#: ui/ApplicationConfiguration.qml:172
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr "Esta aplicación non permite configurar as notificacións por eventos."

#: ui/ApplicationConfiguration.qml:261
#, kde-format
msgid "Show a message in a pop-up"
msgstr "Amosar unha mensaxe emerxente"

#: ui/ApplicationConfiguration.qml:270
#, kde-format
msgid "Play a sound"
msgstr "Reproducir un son"

#: ui/main.qml:41
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"Non foi posíbel atopar un trebello de «Notificacións», necesario para amosar "
"notificacións. Asegúrese de que está activado ben na área de notificación ou "
"nun trebello independente."

#: ui/main.qml:52
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr "Actualmente as notificacións fornéceas «%1 %2» e non Plasma."

#: ui/main.qml:56
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Actualmente as notificacións non as fornece Plasma."

#: ui/main.qml:63
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Modo de non molestar"

#: ui/main.qml:68
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "Enable:"
msgstr "Activar:"

#: ui/main.qml:69
#, kde-format
msgctxt "Enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "Cando as pantallas están reflectidas"

#: ui/main.qml:81
#, kde-format
msgctxt "Enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "Durante a compartición de pantalla"

#: ui/main.qml:96
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Keyboard shortcut:"
msgstr "Atallo de teclado:"

#: ui/main.qml:103
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Condicións de visibilidade"

#: ui/main.qml:108
#, kde-format
msgid "Critical notifications:"
msgstr "Notificacións críticas:"

#: ui/main.qml:109
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Amosar no modo de non molestar"

#: ui/main.qml:121
#, kde-format
msgid "Normal notifications:"
msgstr "Notificacións normais:"

#: ui/main.qml:122
#, kde-format
msgid "Show over full screen windows"
msgstr "Amosar sobre as xanelas a pantalla completa"

#: ui/main.qml:134
#, kde-format
msgid "Low priority notifications:"
msgstr "Notificacións de prioridade baixa:"

#: ui/main.qml:135
#, kde-format
msgid "Show popup"
msgstr "Amosar a xanela emerxente"

#: ui/main.qml:164
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Xanelas emerxentes"

#: ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Localización:"

#: ui/main.qml:171
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Canda a icona da notificación"

#: ui/main.qml:208
#, kde-format
msgid "Choose Custom Position…"
msgstr "Escoller unha posición personalizada…"

#: ui/main.qml:217 ui/main.qml:233
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 segundo"
msgstr[1] "%1 segundos"

#: ui/main.qml:222
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Agochar tras:"

#: ui/main.qml:245
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Información adicional"

#: ui/main.qml:251
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Amosar nas notificacións"

#: ui/main.qml:252
#, kde-format
msgid "Application progress:"
msgstr "Progreso da aplicación:"

#: ui/main.qml:266
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "Manter a xanela emerxente aberta durante o progreso"

#: ui/main.qml:279
#, kde-format
msgid "Notification badges:"
msgstr "Emblemas das notificacións:"

#: ui/main.qml:280
#, kde-format
msgid "Show in task manager"
msgstr "Amosar no xestor de tarefas"

#: ui/main.qml:291
#, kde-format
msgctxt "@title:group"
msgid "Application-specific settings"
msgstr "Configuración específica de aplicacións"

#: ui/main.qml:296
#, kde-format
msgid "Configure…"
msgstr "Configurar…"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Posición da xanela emerxente"

#: ui/SourcesPage.qml:19
#, kde-format
msgid "Application Settings"
msgstr "Configuración da aplicación"

#: ui/SourcesPage.qml:99
#, kde-format
msgid "Applications"
msgstr "Aplicacións"

#: ui/SourcesPage.qml:100
#, kde-format
msgid "System Services"
msgstr "Servizos do sistema"

#: ui/SourcesPage.qml:148
#, kde-format
msgid "No application or event matches your search term"
msgstr "Ningunha aplicación ou evento coincide co termo de busca."

#: ui/SourcesPage.qml:171
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr ""
"Seleccione unha aplicación da lista para configurar as súas opcións e "
"comportamento."

#~ msgid "Configure Notifications"
#~ msgstr "Configurar as notificacións"

#~ msgid "This module lets you manage application and system notifications."
#~ msgstr ""
#~ "Este módulo permítelle xestionar as notificacións de aplicacións e do "
#~ "sistema."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Adrian Chaves (Gallaecio)"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "adrian@chaves.io"

#~ msgid "Notifications"
#~ msgstr "Notificacións"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"

#~ msgid "Show critical notifications"
#~ msgstr "Mostrar notificacións críticas"

#~ msgid "Always keep on top"
#~ msgstr "Manter sempre arriba"

#~ msgid "Popup position:"
#~ msgstr "Posición da mensaxe emerxente:"
