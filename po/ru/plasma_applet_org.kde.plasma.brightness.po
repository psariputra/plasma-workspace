# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Alexander Yavorsky <kekcuha@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-20 01:35+0000\n"
"PO-Revision-Date: 2023-12-03 20:42+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.3\n"

#: package/contents/ui/BrightnessItem.qml:70
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/main.qml:86
#, kde-format
msgid "Brightness and Color"
msgstr "Яркость и цвет"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Screen brightness at %1%"
msgstr "Яркость экрана: %1%"

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "Яркость клавиатуры: %1%"

#: package/contents/ui/main.qml:105
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr "Ночная цветовая схема отключена"

#: package/contents/ui/main.qml:107
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr "Цветовая температура ночной цветовой схемы: %1K"

#: package/contents/ui/main.qml:117
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Используйте колесо мыши для настройки яркости экрана"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr "Включить или отключить ночную цветовую схему: щелчок средней кнопкой"

#: package/contents/ui/main.qml:258
#, fuzzy, kde-format
#| msgid "Configure…"
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Настроить…"

#: package/contents/ui/NightColorItem.qml:74
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr "Отключена"

#: package/contents/ui/NightColorItem.qml:77
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Недоступна"

#: package/contents/ui/NightColorItem.qml:80
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Не включена"

#: package/contents/ui/NightColorItem.qml:83
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Не запущена"

#: package/contents/ui/NightColorItem.qml:86
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Включена"

#: package/contents/ui/NightColorItem.qml:89
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Утренний переход"

#: package/contents/ui/NightColorItem.qml:91
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "День"

#: package/contents/ui/NightColorItem.qml:93
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Вечерний переход"

#: package/contents/ui/NightColorItem.qml:95
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Ночь"

#: package/contents/ui/NightColorItem.qml:105
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:139
#, kde-format
msgid "Configure…"
msgstr "Настроить…"

#: package/contents/ui/NightColorItem.qml:139
#, kde-format
msgid "Enable and Configure…"
msgstr "Включить и настроить…"

#: package/contents/ui/NightColorItem.qml:165
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Переход к дневной схеме завершён в:"

#: package/contents/ui/NightColorItem.qml:167
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Переход к ночной схеме запланирован в:"

#: package/contents/ui/NightColorItem.qml:169
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Переход к ночной схеме завершён в:"

#: package/contents/ui/NightColorItem.qml:171
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Переход к дневной схеме запланирован в:"

#: package/contents/ui/PopupDialog.qml:70
#, kde-format
msgid "Display Brightness"
msgstr "Яркость экрана"

#: package/contents/ui/PopupDialog.qml:101
#, kde-format
msgid "Keyboard Brightness"
msgstr "Яркость клавиатуры"

#: package/contents/ui/PopupDialog.qml:132
#, kde-format
msgid "Night Light"
msgstr "Ночная цветовая схема"
